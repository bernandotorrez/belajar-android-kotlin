package com.bernand.barvolumekotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var BtnHitung: Button
    private lateinit var EditPanjang: EditText
    private lateinit var EditLebar: EditText
    private lateinit var EditTinggi: EditText
    private lateinit var TVResult: TextView

    companion object {
        private const val STATE_RESULT = "state_result"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        EditPanjang = findViewById(R.id.et_panjang)
        EditLebar = findViewById(R.id.et_lebar)
        EditTinggi = findViewById(R.id.et_tinggi)
        BtnHitung = findViewById(R.id.btn_hitung)
        TVResult = findViewById(R.id.tv_hasil)

        BtnHitung.setOnClickListener {
            val inputPanjang = EditPanjang.text.toString().trim()
            val inputLebar = EditLebar.text.toString().trim()
            val inputTinggi = EditTinggi.text.toString().trim()

            var isEmpty = false

            if(inputPanjang.isEmpty()) {
                isEmpty = true
                EditPanjang.error = "Input Panjang harus di isi"
            }

            if(inputLebar.isEmpty()) {
                isEmpty = true
                EditLebar.error = "Input Lebar harus di isi"
            }

            if(inputTinggi.isEmpty()) {
                isEmpty = true
                EditTinggi.error = "Input panjang harus di isi"
            }

            if(isEmpty == false) {
                val result = inputPanjang.toDouble() * inputLebar.toDouble() * inputTinggi.toDouble()

                TVResult.text = result.toString()
            }

        }

        if(savedInstanceState != null) {
            val result = savedInstanceState.getString(STATE_RESULT).toString()
            TVResult.text = result
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString(STATE_RESULT, TVResult.text.toString())
    }
}
